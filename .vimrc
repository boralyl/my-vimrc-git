" Load pathogen modules
call pathogen#infect()

" Enable line numbers
set number

" Enable mouse clicking
set mouse=a

" Always show the current position
set ruler

" Highlight search terms
set hlsearch

" Search as you type like in browsers
set incsearch

"Ignore case when searching
set ignorecase 

filetype plugin on
filetype indent on

" Toggle paste on/off.  With it on formatting doesn't get messed up when
" pasting from an external application
set pastetoggle=<F2>

" Don't outdent hashes
inoremap # X<BS>#

" Turn on smart indent
set smartindent
set smarttab
set tabstop=4 " set tab character to 4 characters
set expandtab " turn tabs into whitespace
set shiftwidth=4 " indent width for autoindent
set softtabstop=4 " makes backspace go back 4 spaces
set autoindent

" Show filename always
set ls=2

" Set color scheme
set t_Co=256 " Use all 256 colors
colorscheme molokai

" Show vertical line at col 80
if exists('+colorcolumn')
    set colorcolumn=80
else
    au BufWinEnter * let w:m2=matchadd('ErrorMsg', '\%>80v.\+', -1)
endif

" Remap CommanT from `\t` to` \`
noremap \ :CtrlP<CR>
" Commented out as it needs ruby compiled in vim.  If that doesn't
" exist we can use CtrlP instead.
" noremap \ :CommandT<CR

" Tab configuration shortcuts
map <leader>tn :tabnew<cr>
map <leader>te :tabedit
map <leader>tc :tabclose<cr>

" Move a line of text using ALT+[jk]
nmap <M-j> mz:m+<cr>`z
nmap <M-k> mz:m-2<cr>`z
vmap <M-j> :m'>+<cr>`<my`>mzgv`yo`z
vmap <M-k> :m'<-2<cr>`>my`<mzgv`yo`z

" Delete trailing white space, useful for Python ;)
func! DeleteTrailingWS()
    exe "normal mz"
    %s/\s\+$//ge
    exe "normal `z"
endfunc
autocmd BufWrite *.py :call DeleteTrailingWS()

" Use system clipboard for yank, paste, cut, etc.
set clipboard+=unnamed

" gvim specific gui options
set guioptions-=m  "remove menu bar
set guioptions-=T  "remove toolbar

"""""""""""""""""""""""""""""
" => Command-T
"""""""""""""""""""""""""""""
let g:CommandTMaxHeight = 15
set wildignore+=*.o,*.obj,.git,*.pyc
noremap <F5> :CommandTFlush<CR>

" loading ctrlp plugin
set runtimepath^=~/.vim/bundle/ctrlp.vim
